//Betaling
import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
    cy.visit('http://localhost:8080');
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});
And(/^trykket på "Gå til betaling"$/, () => {
    cy.get('#goToPayment').click;
    cy.visit('/payment.html');
});

When (/^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/, () => {
    cy.get('#fullName').type('Robin Salte');
    cy.get('#address').type('Gatenavn 123');
    cy.get('#postCode').type('1234');
    cy.get('#city').type('Bynavn');
    cy.get('#creditCardNo').type('1234567812345678');

});
And(/^trykker på "Fullfør kjøp"$/, () => {
    cy.get('form').find('input[type="submit"]').click();
});
Then(/^skal jeg få beskjed om at kjøpet er registrert$/, () => {
    cy.get('.confirmation').should('contain', 'Din ordre er registrert.');
});

When (/^jeg legger inn ugyldige verdier i feltene$/, () => {
    cy.get('#fullName').clear().blur();
        cy.get('#address').clear().blur();
        cy.get('#postCode').clear().blur();
        cy.get('#city').clear().blur();
        cy.get('#creditCardNo').clear().blur();

        
});

Then(/^skal jeg få feilmeldinger for disse$/, () => {
    // Verifiser at de riktige feilmeldingene vises
cy.get('#fullNameError').should('have.text', 'Feltet må ha en verdi');
cy.get('#addressError').should('have.text', 'Feltet må ha en verdi');
cy.get('#postCodeError').should('have.text', 'Feltet må ha en verdi');
cy.get('#cityError').should('have.text', 'Feltet må ha en verdi');
cy.get('#creditCardNoError').should('have.text', 'Kredittkortnummeret må bestå av 16 siffer');
});
