import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

When(/^jeg legger inn varer og kvanta$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

Then(/^skal handlekurven inneholde det jeg har lagt inn$/, () => {
   cy.get('#list li').should('have.length', 4); // 4 varer
    // Sjekker at produktene er lagt til med riktig kvantitet og pris
    cy.get('#list').should('contain', 'Stratos; 8 kr')
                    .and('contain', 'Hubba bubba; 8 kr')
                    .and('contain', 'Smørbukk; 5 kr')
                    .and('contain', 'Hobby; 12 kr');
});



And(/^den skal ha riktig totalpris$/, function () {
        cy.get('#price').should('have.text', '33');
});

//2)
And(/^lagt inn varer og kvanta$/, function () {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

When(/^jeg sletter varer$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#deleteItem').click();
});

Then(/^skal ikke handlekurven inneholde det jeg har slettet$/, () => {
    cy.get('#list li').should('have.length', 1);
    cy.get('#list').should('contain', 'Smørbukk; 2 kr');
});

// 3)

When(/^jeg oppdaterer kvanta for en vare$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('8');
    cy.get('#saveItem').click();
});

Then(/^skal handlekurven inneholde riktig kvanta for varen$/, () => {
    cy.get('#list li').should('have.length', 2);
    cy.get('#list').should('contain', '8 Hubba bubba; 16 kr')
    .and('contain', 'Smørbukk; 2 kr');
});

